#!/bin/bash
if [ $# -eq 0 ]; then
   /usr/games/fortune | /usr/games/cowsay -f duck
else
   /usr/games/cowsay "$@"
fi
